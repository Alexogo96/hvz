package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.Squad;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SquadRepository extends JpaRepository<Squad, Integer> {

    Squad findSquadBygameIdAndId(int gameId, int id);

    List<Squad> findSquadBygameId(int id);

}
