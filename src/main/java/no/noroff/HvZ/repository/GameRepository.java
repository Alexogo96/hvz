package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game, Integer> {
    Game findGameById(int id);
}
