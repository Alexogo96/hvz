package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.Kills;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface KillRepository extends JpaRepository<Kills, Integer> {

    Kills findKillById(int id);

    ArrayList<Kills> findKillsByGameId(int game_id);

    Kills findBygameIdAndId(int game_id, int kill_id);
}
