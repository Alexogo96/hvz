package no.noroff.HvZ.repository;


import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.model.SquadMember;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface SquadMemberRepository extends JpaRepository<SquadMember, Integer> {
    List<SquadMember> findSquadMemberBygameIdAndId(int gameId, int id);
    boolean existsBygameIdAndSquadId(int gameId, int squadId);
    List<SquadMember> findSquadMemberBygameIdAndSquadId(int gameId, int squadId);

    boolean existsBygameIdAndSquadIdAndSquadMember(int gameId, int squadId, Player SquadMember);

    SquadMember findBygameIdAndSquadMember(int gameId, Player SquadMember);

    boolean existsBygameIdAndSquadMember(int gameId, Player SquadMember);
}