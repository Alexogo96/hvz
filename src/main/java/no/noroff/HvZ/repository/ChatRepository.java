package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface ChatRepository extends JpaRepository<Chat, Integer> {
    Chat findChatById(int id);

    ArrayList<Chat> findChatBygameID(int gameID);

    @Query("SELECT a FROM Chat a WHERE gameId = ?1 AND isZombie = ?2 AND isHuman = ?3")
    ArrayList<Chat> findChatByGameIDAndIsZombieAndiIsHuman(int gameId, boolean isZombie, boolean isHuman);

    ArrayList<Chat> findChatByGameIDAndSquadId(int gameID, int squadId);
}
