package no.noroff.HvZ.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import no.noroff.HvZ.config.View;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int id;

    @NotNull
    @Column(name = "Username", unique = true)
    public String username;
    
    @NotNull
    @Column(name = "password")
    public String password;

    @OneToMany(mappedBy = "userId", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Player> Players = new ArrayList<Player>();

    @NotNull
    @Column(name = "role")
    public String role;  // ROLE_USER and ROLE_ADMIN
}
