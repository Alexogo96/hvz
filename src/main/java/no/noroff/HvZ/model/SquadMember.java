package no.noroff.HvZ.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import no.noroff.HvZ.config.View;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table
public class SquadMember {
    //@JsonView(View.Admin.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "squad_member_id")
    public int id;

    @NotNull
    @Column(name = "squad_member_rank")
    public boolean squadMemberRank;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name = "squad_id"))
    public int squadId;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name = "game_id"))
    public int gameId;

    @JsonIgnoreProperties("squadMember")
    @OneToOne(cascade = CascadeType.PERSIST, orphanRemoval = false)
    @JoinColumn(nullable = false, name="player_id", referencedColumnName = "player_id")
    public Player squadMember;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "squadMemberId", orphanRemoval = true)
    public List<SquadCheckIn> squadCheckInList;
}