package no.noroff.HvZ.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.sun.istack.Nullable;
import no.noroff.HvZ.config.View;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table
public class Chat {

    @JsonView(View.Admin.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")
    public int id;

    
    @NotNull
    @Column(name="username")
    public String username;


    @NotNull
    @Column(name = "message")
    public String message;

    @NotNull
    @Column(name = "is_human")
    public boolean isHuman;

    @NotNull
    @Column(name = "is_zombie")
    public boolean isZombie;

    @NotNull
    //@Temporal(TemporalType.TIME)
    @Column(name = "chat_time")
    public java.sql.Time sqlTime;

    @Nullable
    @Column(name="squad_id")
    public int squadId;

    @NotNull
    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name="game_id"))
    public int gameID;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name="player_id"))
    public int playerId;


}
