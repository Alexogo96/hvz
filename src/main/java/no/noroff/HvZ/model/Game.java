package no.noroff.HvZ.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "game_id")
    public int id;

    @Column(name = "name")
    public String name;

    @Column(name = "game_state")
    public String gameState;

    @Column(name = "longitude")
    public double longitude;

    @Column(name = "latitude")
    public double latitude;

    @Column(name = "is_north_west")
    public boolean is_north_west;

    @Column(name="start_date")
    public java.sql.Date startDate;

    @Column(name="description")
    public String description;


    @OneToMany(mappedBy = "gameId", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    //@JoinColumn(name = "game_id", referencedColumnName = "game_id")
    public List<Kills> killsList;

    @OneToMany(mappedBy = "gameId", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    //@JoinColumn(name = "game_id", referencedColumnName = "game_id")
    public List<Mission> missionList = new ArrayList<>();

    @OneToMany(mappedBy = "gameId", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    //@JoinColumn(name = "game_id", referencedColumnName = "game_id")
    public List<Player> playerList;

    @OneToMany(mappedBy = "gameID" ,cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    //@JoinColumn(name = "game_id", referencedColumnName = "game_id")
    public List<Chat> chatList;

    @OneToMany(mappedBy = "gameId", orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    //@JoinColumn(name = "game_id", referencedColumnName = "game_id")
    public List<Squad> squadList;
}
