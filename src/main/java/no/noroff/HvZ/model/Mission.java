package no.noroff.HvZ.model;

import com.fasterxml.jackson.annotation.JsonView;
import no.noroff.HvZ.config.View;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Mission {

    @JsonView(View.Admin.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mission_id")
    public int id;

    @NotNull
    @Column(name = "mission_name")
    public String missionName;

    @NotNull
    @Column(name = "is_human_faction")
    public boolean isHumanFaction;

    @NotNull
    @Column(name = "is_zombie_faction")
    public boolean isZombieFaction;

    @Column(name = "mission_description")
    public String missionDescription;

    @Column(name = "start_date")
    public java.sql.Date startDate;

    @Column(name = "end_date")
    public java.sql.Date endDate;

    @NotNull
    @Column(name = "lat")
    public double lat;

    @NotNull
    @Column(name = "lng")
    public double lng;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name="game_id"))
    public int gameId;
}