package no.noroff.HvZ.config;

import no.noroff.HvZ.security.CustomUserDetailsService;
import no.noroff.HvZ.security.JwtAuthenticationEntryPoint;
import no.noroff.HvZ.security.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/*
 * Created by rajeevkumarsingh on 01/08/17.
 */

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http

                .cors()
                    .and()
                .csrf()
                    .disable()
                .exceptionHandling()
                    .authenticationEntryPoint(unauthorizedHandler)
                    .and()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .anonymous().and()
                .authorizeRequests()
                    .antMatchers("/",
                        "/index",
                        "/index.html",
                        "/favicon.ico",
                        "/*/*.png",
                        "/*/*.gif",
                        "/*/*.svg",
                        "/*/*.jpg",
                        "/*/*.html",
                        "/*/*.css",
                        "/*/*.map",
                        "/*/*.js",
                        "/build/static/js/*.js")
                        .permitAll()
                    .antMatchers("/user/login", "/user/register")
                        .permitAll()
                    .antMatchers(HttpMethod.GET,"/game")
                        .permitAll()
                    .antMatchers(HttpMethod.GET, "/game/users")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/game")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET,"/game/*/")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.PUT, "/game/*/")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/game/*/")
                        .hasRole("ADMIN")
                    .antMatchers( "/game/*/chat")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.GET,"/game/*/player/*/")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.PUT,"/game/*/player/*/")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.DELETE,"/game/*/player/*/")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST,"/game/*/kill")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.GET, "/game/*/kill/*/")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.PUT, "/game/*/kill/*/")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/game/*/kill/*/")
                        .hasRole("ADMIN")
                    .antMatchers("/game/*/squad")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.GET, "/game/*/squad/*/")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.POST, "/game/*/squad/*/join")
                        .hasAnyRole("USER","ADMIN")
                    .antMatchers(HttpMethod.PUT, "/game/*/squad/*/")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/game/*/squad/*/")
                        .hasRole("ADMIN")
                    .antMatchers("/game/*/squad/*/chat")
                        .hasAnyRole("ADMIN","USER")
                    .antMatchers("/game/*/squad/*/check-in")
                        .hasAnyRole("ADMIN","USER")
                    .antMatchers(HttpMethod.GET, "/game/*/mission")
                        .hasAnyRole("ADMIN","USER")
                    .antMatchers(HttpMethod.POST, "/game/*/mission")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/game/*/mission/*/")
                        .hasAnyRole("ADMIN","USER")
                    .antMatchers(HttpMethod.PUT, "/game/*/mission/*/")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/game/*/mission/*/")
                        .hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/authentication/role")
                        .hasAnyRole("ADMIN","USER")
                    .antMatchers(HttpMethod.GET, "/game/users/*/")
                        .hasAnyRole("ADMIN","USER")
                    .antMatchers(HttpMethod.DELETE, "/game/*/squad/*/leave")
                        .hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.POST, "/user/*/makeAdmin")
                        .hasAnyRole("ADMIN")
                    .anyRequest()
                        .authenticated();

        // Add our custom JWT security filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}