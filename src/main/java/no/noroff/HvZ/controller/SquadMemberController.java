package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.model.Squad;
import no.noroff.HvZ.model.SquadMember;
import no.noroff.HvZ.repository.PlayerRepository;
import no.noroff.HvZ.repository.SquadMemberRepository;
import no.noroff.HvZ.repository.SquadRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SquadMemberController {

    @Autowired
    private final SquadMemberRepository squadMemberRepository;

    @Autowired
    private final PlayerRepository playerRepository;

    @Autowired
    private  final SquadRepository squadRepository;

    private final Squad squadMember_error = new Squad();

    public SquadMemberController(SquadMemberRepository squadMemberRepository, PlayerRepository playerRepository, SquadRepository squadRepository) {
        this.squadMemberRepository = squadMemberRepository;
        this.playerRepository = playerRepository;
        this.squadRepository = squadRepository;
    }

    @CrossOrigin
    @PostMapping("/game/{game_id}/squad/{squad_id}/join")
    public ResponseEntity addSquadMember(@PathVariable(name = "game_id") int gameId, @PathVariable(name = "squad_id") int squadId, @RequestBody SquadMember squadMember, Authentication authentication) {
        if (!squadMemberRepository.existsBygameIdAndSquadId(gameId, squadId)) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(squadMember_error);
        }

        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        Player p = playerRepository.findPlayerByGameIdAndUserId(gameId, Math.toIntExact(principal.getId()));

        if(p==null){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(squadMember_error);
        }

        if (!p.isHuman) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(squadMember_error);
        }

        if (squadMemberRepository.existsBygameIdAndSquadMember(gameId, p))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(squadMember_error);

        squadMember.id = 0;
        squadMember.squadMemberRank = false;
        squadMember.squadId = squadId;
        squadMember.gameId = gameId;

        p.squadId = squadId;

        playerRepository.save(p);
        squadMember.squadMember = p;

        squadMemberRepository.save(squadMember);
        return ResponseEntity.status(HttpStatus.OK).body(squadMember);
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/squad/{squad_id}/members")
    public ResponseEntity SquadMembers(@PathVariable(name = "game_id") int gameId, @PathVariable(name = "squad_id") int squadId) {
        List<SquadMember> sml = squadMemberRepository.findSquadMemberBygameIdAndSquadId(gameId, squadId);

        return ResponseEntity.status(HttpStatus.OK).body(sml);
    }

    @CrossOrigin
    @DeleteMapping("/game/{game_id}/squad/{squad_id}/leave")
    public ResponseEntity DeleteSquadMember(@PathVariable int game_id, @PathVariable int squad_id ,Authentication authentication) {
        UserPrincipal userPrincipal  = (UserPrincipal) authentication.getPrincipal();
        Player player = playerRepository.findPlayerByGameIdAndUserId(game_id, Math.toIntExact(userPrincipal.getId()));

        if (player == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(squadMember_error);

        if (player.squadId != squad_id) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(squadMember_error);
        }

        player.squadId = 0;
        playerRepository.save(player);

        SquadMember squadmember = squadMemberRepository.findBygameIdAndSquadMember(game_id, player);
        squadMemberRepository.delete(squadmember);

        List<SquadMember> squadMembers = squadMemberRepository.findSquadMemberBygameIdAndSquadId(game_id, squad_id);

        if (squadMembers.size() == 0) {
            squadRepository.delete(squadRepository.findSquadBygameIdAndId(game_id, squad_id));
        } else {
                SquadMember sm = squadMembers.get(0);
                sm.squadMemberRank = true;
                squadMemberRepository.save(sm);
            }
        return ResponseEntity.status(HttpStatus.OK).body(squadmember);
    }
}


