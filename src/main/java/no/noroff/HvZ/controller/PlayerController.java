package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.model.User;
import no.noroff.HvZ.repository.PlayerRepository;
import no.noroff.HvZ.repository.UserRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

//temp import

@RestController
public class PlayerController {

    private final Player player_error = new Player();

    @Autowired
    private final PlayerRepository playerRepository;
    private final UserRepository userRepository;




    public PlayerController(PlayerRepository playerRepository, UserRepository userRepository) {
        this.playerRepository = playerRepository;
        this.userRepository = userRepository;
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/player")
    public ResponseEntity findAllPlayers(@PathVariable int game_id) {
        List<Player> player_List = playerRepository.findPlayerBygameId(game_id);
        for(Player p : player_List){
            p.isPatientZero = false;
            p.biteCode = 0;
        }
        return ResponseEntity.ok(player_List);
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/player/{player_id}")
    public ResponseEntity findPlayer(@PathVariable int game_id, @PathVariable int player_id) {
        Player p = playerRepository.findBygameIdAndId(game_id, player_id);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        User u = userRepository.findByUsername(username);
        if(p.userId == u.id || Arrays.toString(authorities.toArray()).equals("[ROLE_ADMIN]")){
            return ResponseEntity.ok(p);
        }else{
            p.biteCode = 0;
            p.isPatientZero = false;
            return ResponseEntity.ok(p);
        }
    }

    @CrossOrigin
    @PostMapping("/game/{game_id}/player")
    public ResponseEntity addPlayer(@PathVariable(name = "game_id") int gameId, @RequestBody Player player, Authentication authentication) {
        //DONE: add logic for handling ADMIN vs non-ADMIN case
        UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int userID = Math.toIntExact(principal.getId());

        if(playerRepository.existsPlayerByGameIdAndUserId(gameId, userID)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(playerRepository.findPlayerByGameIdAndUserId(gameId, userID));
        }

        int biteCode;
        player.userId = userID;
        player.squadId = 0;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        //if player.username == null then we can assume that no custom player object was sent from front-end
        //and thus we should just set default values.
        if(Arrays.toString(authorities.toArray()).equals("[ROLE_ADMIN]")&&player.username!=null){
            do{
                biteCode = new Random().nextInt(899999) + 100000;
                if(!playerRepository.existsByBiteCode(biteCode)){
                    player.biteCode = biteCode;
                }

            }while(playerRepository.existsByBiteCode(biteCode));
            playerRepository.save(player);
            return ResponseEntity.status(HttpStatus.CREATED).body(player);
        }

        player.username = principal.getUsername();

        player.id = 0;
        player.isHuman = true;
        player.isPatientZero = false;
        player.gameId = gameId;


        do {
            biteCode = new Random().nextInt(899999) + 100000;
            if(!playerRepository.existsByBiteCode(biteCode)){
                player.biteCode = biteCode;
            }
        }
        while(playerRepository.existsByBiteCode(biteCode));

        playerRepository.save(player);

        return ResponseEntity.ok(player);
    }

    @CrossOrigin
    @PutMapping("/game/{game_id}/player/{player_id}")
    public ResponseEntity<Player> updatePlayer(@PathVariable int game_id, @PathVariable int player_id, @RequestBody Player player) {

        Player oldPlayer = playerRepository.findBygameIdAndId(game_id, player_id);
        if(oldPlayer == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(player_error);
        }
        oldPlayer.isHuman = player.isHuman;
        oldPlayer.isPatientZero = player.isPatientZero;
        oldPlayer.squadId = player.squadId;
        return ResponseEntity.ok(playerRepository.save(oldPlayer));
    }

    @CrossOrigin
    @DeleteMapping("/game/{game_id}/player/{player_id}")
    public ResponseEntity<Player> deletePlayer(@PathVariable int game_id, @PathVariable int player_id) {

        Player player = playerRepository.findBygameIdAndId(game_id, player_id);
        if(player == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(player_error);
        }
        playerRepository.delete(player);
        return ResponseEntity.ok(player);
    }


}
