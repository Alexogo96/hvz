package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Mission;
import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.repository.MissionRepository;
import no.noroff.HvZ.repository.PlayerRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RestController
public class MissionController {

    private final Mission mission_error = new Mission();

    @Autowired
    private final MissionRepository missionRepository;

    @Autowired
    private final PlayerRepository playerRepository;


    public MissionController(MissionRepository missionRepository, PlayerRepository playerRepository) {
        this.missionRepository = missionRepository;
        this.playerRepository = playerRepository;
    }

    //TESTED
    //GET /game/<game_id>/mission Get a list of missions. Optionally accepts appropriate query parameters. This should only return missions that are faction appropriate.
    @CrossOrigin
    @GetMapping("/game/{gameId}/mission")
    public ResponseEntity getAllMissions(@PathVariable int gameId) { //Request param for human or zombie
        //DONE: change method to get human/zombie from principal user

        UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int user_id = Math.toIntExact((principal).getId());

        List<Mission> gameMissions = missionRepository.findMissionBygameId(gameId); //Get the full list
        Collection<? extends GrantedAuthority> authorities = principal.getAuthorities();
        if(Arrays.toString(authorities.toArray()).equals("[ROLE_ADMIN]")){
            return ResponseEntity.status(HttpStatus.OK).body(gameMissions);
        }

        if (!playerRepository.existsPlayerByGameIdAndUserId(gameId, user_id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mission_error);
        }

        Player p1 = playerRepository.findPlayerByGameIdAndUserId(gameId, user_id);
        boolean ded = !p1.isHuman;

        List<Mission> missionList = new ArrayList<>(); //Instantiate new list

        if(Arrays.toString(authorities.toArray()).equals("[ROLE_ADMIN]")){
            return ResponseEntity.ok(gameMissions);
        }

        for (Mission mission : gameMissions) { //Loop through the list
                if (!ded) { //Check missions factions
                    if (mission.isHumanFaction) missionList.add(mission); //Check if human mission then add
                } else {
                    if (mission.isZombieFaction) missionList.add(mission); //Check if zombie mission then add
                }
            }
        return ResponseEntity.ok(missionList); //Return the list
    }

     //TESTED
    @CrossOrigin
    @GetMapping("/game/{gameId}/mission/{missionId}/")
    public ResponseEntity getMission(@PathVariable int gameId, @PathVariable int missionId, @RequestHeader(value = "isHuman") boolean isHuman) {
        //DONE: change method to get human/zombie from principal user

        UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        int user_id = Math.toIntExact((principal).getId());

        Player p1 = playerRepository.findPlayerByGameIdAndUserId(gameId, user_id);
        boolean ded = !p1.isHuman;

        if(!missionRepository.existsById(missionId)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mission_error);
        }
        Mission m1 = missionRepository.findMissionById(missionId);

        if(ded==m1.isZombieFaction){
            return ResponseEntity.ok(m1);
        }else{
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(mission_error);
        }
    }

    //Tested
    //POST /game/<game_id>/mission Creates a mission object. Accepts appropriate parameters in the request body as application/json.
    //DONE: Admin only.
    @CrossOrigin
    @PostMapping("/game/{gameId}/mission")
    public ResponseEntity createMission(@PathVariable int gameId, @RequestBody Mission mission) {
        mission.gameId = gameId;
        return ResponseEntity.status(HttpStatus.CREATED).body(missionRepository.save(mission));
    }

    //TESTED
    //PUT /game/<game_id>/mission/<mission_id> Updates a mission object. Accepts appropriate parameters in the request body as application/json.
    //DONE: Admin only.
    @CrossOrigin
    @PutMapping("/game/{gameId}/mission/{missionId}/")
    public ResponseEntity updateMission(@PathVariable int gameId, @PathVariable int missionId, @RequestBody Mission mission) {
        return ResponseEntity.status(HttpStatus.OK).body(missionRepository.save(mission));
    }

    //TESTED
    //DELETE /game/<game_id>/mission/<mission_id> Delete a mission.
    //DONE: Admin only.
    @CrossOrigin
    @DeleteMapping("/game/{gameId}/mission/{missionId}/")
    public ResponseEntity deleteMission(@PathVariable int gameId, @PathVariable int missionId) {
        if (missionRepository.existsById(missionId)) {
            Mission mission = missionRepository.findMissionById(missionId);
            missionRepository.deleteById(missionId);
            return ResponseEntity.status(HttpStatus.OK).body(mission);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mission_error);
    }
}
