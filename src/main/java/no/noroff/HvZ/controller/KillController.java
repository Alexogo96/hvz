package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Kills;
import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.repository.KillRepository;
import no.noroff.HvZ.repository.PlayerRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RestController
public class KillController {

    private final Kills kill_error = new Kills();

    @Autowired
    private final KillRepository killRepository;

    @Autowired
    private final PlayerRepository playerRepository;

    public KillController(KillRepository killRepository, PlayerRepository playerRepository) {
        this.killRepository = killRepository;
        this.playerRepository = playerRepository;
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/kill")
    public ResponseEntity getAllKills(@PathVariable int game_id) {

        return ResponseEntity.ok(killRepository.findKillsByGameId(game_id));
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/kill/{kill_id}")
    public ResponseEntity<Kills> getKill(@PathVariable int game_id, @PathVariable int kill_id) {
        Kills kills = killRepository.findBygameIdAndId(game_id, kill_id);

        if (kills != null) return new ResponseEntity<>(kills, HttpStatus.OK);
        return new ResponseEntity<>(kill_error, HttpStatus.BAD_REQUEST);
    }

    @CrossOrigin
    @PostMapping("/game/{game_id}/kill")
    public ResponseEntity addKill(@RequestHeader("BiteCode") int biteCode, @PathVariable int game_id, @RequestBody Kills kill) {
        Player player = playerRepository.findPlayerByBiteCode(biteCode);
        if (player == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(kill_error);
        } else if(!player.isHuman) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(kill_error);
        }

        // register valid kill
        kill.id = 0;
        kill.timeOfDeath = LocalTime.now();
        kill.victim = player;
        kill.gameId = game_id;

        player.isHuman = false;
        return ResponseEntity.ok(killRepository.save(kill));
    }

    @CrossOrigin
    @PutMapping("/game/{game_id}/kill/{kill_id}")
    public ResponseEntity updateKill(@PathVariable int game_id, @PathVariable int kill_id, @RequestBody Kills updatedKill, Authentication authentication) {

        //DONE proper admin check + currentUserId/killer
        //DONE check if the updated kill effects other players kills and dead status
        //TODO Write this shit from scratch.

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Collection<? extends GrantedAuthority> authorities = userPrincipal.getAuthorities();

        Kills oldKill = killRepository.findBygameIdAndId(game_id, kill_id);

        if(oldKill!=null)updatedKill.id = oldKill.id;

        if(Arrays.toString(authorities.toArray()).equals("[ROLE_ADMIN]")){
            if(oldKill==null){
                Kills kill = new Kills();
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(kill);
            }

            if(oldKill.victim.id!=updatedKill.victim.id){
                oldKill.victim.isHuman=true;
                updatedKill.victim.isHuman=false;
                playerRepository.save(oldKill.victim);
            }else{
                updatedKill.victim.isHuman=false;
            }
            Player p = playerRepository.findPlayerById(updatedKill.killerId);
            //p.killsList.add(updatedKill);
            killRepository.delete(oldKill);


            return ResponseEntity.ok(killRepository.save(updatedKill));
        }


        if(oldKill == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Kill_id not found");
        }

        if (oldKill.killerId == Math.toIntExact(userPrincipal.getId())) {
            if(oldKill.gameId!=updatedKill.gameId){
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Can't change gameId of kill");
            }
            if(oldKill.victim.id==updatedKill.victim.id){
                return ResponseEntity.ok(killRepository.save(updatedKill));
            }else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Only admin can change victim_id");
            }

        } else  {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Killer id did not match");
        }
    }

    @CrossOrigin
    @DeleteMapping("/game/{game_id}/kill/{kill_id}")
    public ResponseEntity deleteKill(@PathVariable int game_id, @PathVariable int kill_id) {


        Kills kill = killRepository.findKillById(kill_id);

        if(kill == null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(kill_error);
        }


        Player p = kill.victim;
        p.isHuman = true;
        playerRepository.save(p);
        killRepository.delete(kill);
        return ResponseEntity.ok(kill);
    }
}
