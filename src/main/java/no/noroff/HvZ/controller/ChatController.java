package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Chat;
import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.repository.ChatRepository;
import no.noroff.HvZ.repository.GameRepository;
import no.noroff.HvZ.repository.PlayerRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
public class ChatController {

    private final Chat chat_error = new Chat();

    @Autowired
    private final ChatRepository chatRepository;
    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;

    public ChatController(ChatRepository chatRepository, GameRepository gameRepository, PlayerRepository playerRepository){
        this.chatRepository = chatRepository;
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
    }

    @CrossOrigin
    @GetMapping("/game/{id}/populate")
    public void populate(@PathVariable(name="id")int id){
        Chat chat1 = new Chat();
        chat1.gameID = id;
        chat1.id = 0;
        chat1.isHuman = false;
        chat1.isZombie = true;
        chat1.message = "Chat test 1"+"id: "+id;
        chat1.sqlTime = java.sql.Time.valueOf("15:30:14");

        Chat chat2 = new Chat();
        chat2.gameID = id+1;
        chat2.id=0;
        chat2.isHuman=true;
        chat2.isZombie=true;
        chat2.message = "Chat test 2"+"id: "+id+1;
        chat2.sqlTime = java.sql.Time.valueOf("16:45:00");

        chatRepository.save(chat1);
        chatRepository.save(chat2);

        System.out.println("Worked");

    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/factionChat")
    public ResponseEntity getFactionChat(@PathVariable(name = "game_id") int game_id, Authentication authentication){
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        boolean exist = playerRepository.existsPlayerByGameIdAndUserId(game_id, Math.toIntExact(userPrincipal.getId()));
        if(!exist){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(chat_error);
        }
        boolean isHuman = playerRepository.findPlayerByGameIdAndUserId(game_id, Math.toIntExact(userPrincipal.getId())).isHuman;
        boolean isZombie = !isHuman;
        if(isZombie){
            return ResponseEntity.ok(chatRepository.findChatByGameIDAndIsZombieAndiIsHuman(game_id, true, false));
        }else{
            return ResponseEntity.ok(chatRepository.findChatByGameIDAndIsZombieAndiIsHuman(game_id, false, true));
        }
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/AllChat")
    public ResponseEntity getAllChat(@PathVariable(name = "game_id") int game_id){
        return ResponseEntity.ok(chatRepository.findChatByGameIDAndIsZombieAndiIsHuman(game_id, true, true));
    }

    @CrossOrigin
    @GetMapping("/game/chat")
    public ResponseEntity getMessage(){
        //remember to update when GameID is updated to an actual game object (FK_GAME)
        return ResponseEntity.ok(chatRepository.findAll());
    }

    @CrossOrigin
    @PostMapping(path = "/game/{id}/chat", consumes = "application/json")
    public ResponseEntity newMessage(@PathVariable (name="id")int id, @RequestBody Chat chat, Authentication authentication){

        UserPrincipal principal = (UserPrincipal)authentication.getPrincipal();
        Player p = playerRepository.findPlayerByGameIdAndUserId(id, Math.toIntExact(principal.getId()));
        chat.username = principal.getUsername();
        chat.playerId = p.id;
        LocalTime now = LocalTime.now();
        chat.sqlTime = Time.valueOf(now);

        if(!chat.isHuman&&!chat.isZombie){
            chat.isHuman = true;
            chat.isZombie = true;
        }
        chat.gameID = id;
        chatRepository.save(chat);
        return ResponseEntity.ok(chat);
    }


}
